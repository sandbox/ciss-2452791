(function($) {

  RAPIT.Ui = (function() {

    var infoBox = (function() {

      var config = {
        templates: {
          wrapper:   '<div class="rapit-info">',
          data:      '<div class="rapit-info-data">',
          status:    '<div class="rapit-info-status">'
        },
        draggable: true,
        resizable: { handles: 'e' },
        selectorParent: 'body'
      };

      var tmp = {
        $wrapper: $([]),
        $data:    $([]),
        $status:  $([])
      };

      function init() {
        var width;
        tmp.$wrapper = $(config.templates.wrapper);
        tmp.$data    = $(config.templates.data);
        tmp.$status  = $(config.templates.status);
        tmp.$wrapper.append(tmp.$data, tmp.$status).appendTo( $(config.selectorParent) );
        if(config.draggable) {
          tmp.$wrapper.draggable(typeof config.draggable === 'object' ? config.draggable : {});
        }
        if(config.resizable) {
          width = tmp.$wrapper.outerWidth();
          tmp.$wrapper.resizable($.extend({}, typeof config.resizable === 'object' ? config.resizable : {}, {
            minWidth: width * .8,
            maxWidth: width * 2
          }));
        }
        setBoxType('default');
      }

      function setBoxType(type) {
        tmp.$wrapper.attr('data-rapit-type', type);
      }

      function setData($content, append) {
        if(!append) {
          tmp.$data.empty();
        }
        tmp.$data.append($content);
      }

      function setStatus(type, content) {
        tmp.$status
          .attr('data-rapit-type', type)
          .empty()
          .append(content);
      }

      function getOffset() {
        return tmp.$wrapper.offset();
      }

      function setOffset(type, px) {
        var offset, offsetType;
        switch(type) {
          case 'top':
          case 'left':
            offsetType = type;
            offset = px;
            break;
          case 'right':
            offsetType = 'left';
            offset = $(window).width() - tmp.$wrapper.outerWidth() - px;
            break;
          case 'bottom':
            offsetType = 'top';
            offset = $(window).height() - tmp.$wrapper.outerHeight() - px;
            break;
          default:
            return;
        }
        tmp.$wrapper.css(offsetType, offset + 'px');
      }

      function getWrapper() {
        return tmp.$wrapper;
      }

      return {
        init:       init,
        setData:    setData,
        setStatus:  setStatus,
        setBoxType: setBoxType,
        getOffset:  getOffset,
        setOffset:  setOffset,
        getWrapper: getWrapper
      };

    }());

    var config = {
      itemDefaults: {
        idle:   [ ['ID', ''], ['Context', ''], ['Source', ''] ],
        noData: [ ['ID', '--'], ['Context', '--'], ['Source', '--'] ]
      },
      templates: {
        highlighted:   '<div class="rapit-overlay-highlighted">',
        related:       '<div class="rapit-overlay-related">',
        infoList:      '<ul class="rapit-info-data-list">',
        infoItem:      '<li class="rapit-info-data-item">',
        infoItemLabel: '<span class="rapit-info-data-item-label">',
        infoItemValue: '<span class="rapit-info-data-item-value">',
        infoParamList: '<ul class="rapit-info-data-param-list">',
        infoParamItem: '<li class="rapit-info-data-param-item">',
        infoParamItemLabel: '<span class="rapit-info-data-param-item-label">',
        infoParamItemValue: '<span class="rapit-info-data-param-item-value">'
      },
      messages: {
        help: 'Hold Shift and move your pointer over text to highlight it.',
        wait: 'Loading data ...'
      }
    };

    var tmp = {
      $highlighted: $([]),
      $related:     $([]),
      translation:  null,
      initialized:  false,
      eventQueue:   [],
      hidden:       false,
      offset:       null
    };

    var helpers = {

      getRectCss: function(rect, absolute) {
        var scroll = { left: 0, top: 0 };
        if(absolute) {
          // Credit and explanation: http://stackoverflow.com/a/3464890
          var doc = document.documentElement;
          scroll.left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
          scroll.top  = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
        }
        return {
          width:  rect.width + "px",
          height: rect.height + "px",
          left:   (rect.left + scroll.left) + "px",
          top:    (rect.top + scroll.top) + "px"
        };
      },

      setInfoDataItems: function(items, params) {
        infoBox.setData(helpers.createDataList(items));
        if(params && params.length) {
          infoBox.setData(helpers.createParamList(params), true);
        }
      },

      createDataList: function(items) {
        var $list = $(config.templates.infoList);
        var item, c = 0;
        while(item = items[c++]) {
          $(config.templates.infoItem).append(
            $(config.templates.infoItemLabel).text(item[0]).attr('title', item[0]),
            $(config.templates.infoItemValue).text(item[1])
          ).appendTo($list);
        }
        return $list;
      },

      createParamList: function(params) {
        var $list = $(config.templates.infoParamList), $item, i = 0, p;
        while(p = params[i++]) {
          $(config.templates.infoParamItem).append(
            $(config.templates.infoParamItemLabel).text(p.id).attr('title', p.id),
            $(config.templates.infoParamItemValue).text(p.range.toString())
          ).appendTo($list);
        }
        return $list;
      },

      clearInfoData: function() {
        infoBox.setData($([]));
      },

      setOverlay: function(translation, template) {
        var css = helpers.getRectCss(translation.getRect(), true);
        return $(template).css(css).appendTo(document.body);
      },

      setHighlighted: function(translation) {
        helpers.clearHighlighted();
        tmp.$highlighted = helpers.setOverlay(translation, config.templates.highlighted);
      },

      clearHighlighted: function() {
        tmp.$highlighted.remove();
        tmp.$highlighted = $([]);
      },

      setRelated: function(translation) {
        var r, i = 0;
        var related = translation ? RAPIT.registry.getTranslations(translation.lid) : RAPIT.registry.getTranslations();
        helpers.clearRelated();
        while(r = related[i++]) {
          if(r !== translation) {
            tmp.$related = tmp.$related.add (helpers.setOverlay(r, config.templates.related) );
          }
        }
      },

      clearRelated: function() {
        tmp.$related.remove();
        tmp.$related = $([]);
      }

    };


    function setTranslation(translation, update) {
      var source, items;

      if(translation == tmp.translation && !update) {
        return;
      }

      tmp.translation = translation;
      if(!translation) {
        helpers.clearInfoData();
        helpers.clearHighlighted();
        helpers.clearRelated();
        helpers.setInfoDataItems(config.itemDefaults.idle);
        return;
      }

      helpers.setHighlighted(translation);
      helpers.setRelated(translation);
      if(source = RAPIT.registry.getSourceSet(translation.lid)) {
        items = [
          ['ID', source.lid],
          ['Context', source.context],
          ['Source', source.source]
        ];
      }
      else {
        items = config.itemDefaults.noData;
        RAPIT.registry.updateSourceSets();
      }
      helpers.setInfoDataItems(items, translation.params);
    }

    // @todo Needs more love.
    function init() {
      if(tmp.initialized) {
        return;
      }
      tmp.initialized = true;
      infoBox.init();
      infoBox.setBoxType('red');
      infoBox.setStatus('help', config.messages.help);
      infoBox.setOffset('left', 35);
      infoBox.setOffset('bottom', 150);
      helpers.setInfoDataItems(config.itemDefaults.idle);

      $(document).bind('mousemove.rapitDemo keydown.rapitDemo', function(event) {
        if(event.shiftKey) {
          var offset      = RAPIT.tracker.getOffset();
          var translation = RAPIT.registry.getTranslationByOffset(offset.x, offset.y);
          setTranslation(translation);
        }
        else {
          setTranslation(null);
        }
      });
      $(document).bind('keyup.rapitdemo', function(event) {
        setTranslation(null);
      });

      var boxHovered = false;

      infoBox.getWrapper().bind({
        'mouseenter.rapitDemo': function (e) {
          boxHovered = true;
          if (e.shiftKey) {
            helpers.setRelated();
          }
        },
        'mouseleave.rapitDemo': function (e) {
          boxHovered = false;
          helpers.clearRelated();
        },
        'mousemove.rapitDemo': function (e) {
          e.shiftKey ? helpers.setRelated() : helpers.clearRelated();
        }
      });

      $(document).bind({
        'keydown.rapitDemoInfo': function(e) {
          if (boxHovered && e.shiftKey) {
            helpers.setRelated();
          }
        },
        'keyup.rapitDemoInfo': function(e) {
          if (boxHovered) {
            helpers.clearRelated();
          }
        }
      });

      $(document).bind({
        'rapitRegistryUpdateStarted': function() {
          infoBox.setStatus('wait', config.messages.wait);
        },
        'rapitRegistryUpdateFinished': function() {
          infoBox.setStatus('help', config.messages.help);
          if(tmp.translation) {
            setTranslation(tmp.translation, true);
          }
        }
      });
    }

    function sync(referenceUi) {
      var offset = referenceUi.getOffset();
      infoBox.setOffset('left', offset.left);
      infoBox.setOffset('top', offset.top);
    }

    function hide() {
      if(!tmp.hidden) {
        tmp.offset = infoBox.getOffset();
        infoBox.getWrapper().hide();
        tmp.hidden = true;
      }
    }

    function show() {
      if(tmp.hidden) {
        tmp.offset = null;
        infoBox.getWrapper().show();
        tmp.hidden = false;
      }
    }

    function disable() {
      infoBox.setBoxType('grey');
    }

    function enable() {
      infoBox.setBoxType('red');
    }

    function getOffset() {
      return tmp.hidden ? tmp.offset : infoBox.getOffset();
    }

    return {
      init: init,
      sync: sync,
      hide: hide,
      show: show,
      disable: disable,
      enable:  enable,
      getOffset: getOffset
    };

  }());


}(jQuery));

(function($){

  var bounds = null;

  Drupal.behaviors.rapitInitialize = {
    attach: function(context, settings) {

      if(context === document) {
        RAPIT.tracker.init();
        RAPIT.Ui.init();

        if(document === top.document) {
          $(document).bind({
            'drupalOverlayOpen': function() {
              RAPIT.Ui.disable();
            },
            'drupalOverlayReady': function() {
              RAPIT.Ui.hide();
            },
            'drupalOverlayBeforeClose': function() {
              RAPIT.Ui.sync(Drupal.overlay.iframeWindow.RAPIT.Ui);
            },
            'drupalOverlayClose': function() {
              RAPIT.Ui.enable();
              RAPIT.Ui.show();
            }
          });
        }
        else {
          RAPIT.Ui.sync(top.window.RAPIT.Ui);
          $(document).bind({
            'drupalOverlayBeforeLoad': function() {
              RAPIT.Ui.disable();
            }
          });
        }
      }

      if(!bounds && settings.Rapit && settings.Rapit.bounds) {
        bounds = settings.Rapit.bounds
      }

      RAPIT.parser.parse(context, bounds);
      RAPIT.registry.updateSourceSets();
    }
  };

  Drupal.admin = Drupal.admin || {};
  Drupal.admin.behaviors = Drupal.admin.behaviors || {};
  Drupal.admin.behaviors.rapitInitialize = Drupal.behaviors.rapitInitialize.attach;

}(jQuery));