RAPIT = window.RAPIT || {};

function RapitException(message) {
  this.name = 'RapitException';
  this.message = message;
}
RapitException.prototype = new Error();
RapitException.prototype.constructor = RapitException;


/**
 * Base objects
 */
(function(R) {

  R.SourceSet = function (lid, context, source) {
    this.lid = lid;
    this.context = context;
    this.source = source;
  };

  //R.ParamSet = function (lid, paramsId, params) {
  //  this.lid = lid;
  //  this.paramsId = paramsId;
  //  this.params = params;
  //};

  R.Translation = function (lid, params, start, end) {
    this.lid = lid;
    this.params = params;
    this.start = start;
    this.end = end;

    this.validate();
    this.range = document.createRange();
    this.range.setStart(this.start, 0);
    this.range.setEnd(this.end, this.end.textContent.length);
  };

  R.Translation.prototype.validate = function () {
    if (!this.start || !this.start.nodeType || this.start.nodeType !== Node.TEXT_NODE) {
      throw new RapitException('Translation.start is not a valid node.');
    }
    if (!this.end || !this.end.nodeType || this.end.nodeType !== Node.TEXT_NODE) {
      throw new RapitException('Translation.end is not a valid node.');
    }
    if (!this.start.parentNode) {
      throw new RapitException('Translation.start has no parent node.');
    }
    if (!this.end.parentNode) {
      throw new RapitException('Translation.end has no parent node.');
    }
  };

  R.Translation.prototype.getRect = function () {
    return this.range.getBoundingClientRect();
  };

  R.Translation.prototype.matchOffset = function (x, y) {
    var rect = this.getRect();
    return (x > rect.left && x < rect.right && y > rect.top && y < rect.bottom);
  };
  R.Translation.prototype.getOffsetScoreX = function () {
    var
      rect  = this.getRect(),
      width = rect.right - rect.left,
      left  = 100 / width * rect.left,
      right = 100 / width * rect.right;
    return left + right;
  };

  R.Param = function (id, start, end) {
    this.id = id;
    this.start = start;
    this.end = end;

    this.validate();
    this.range = document.createRange();
    this.range.setStart(this.start, 0);
    this.range.setEnd(this.end, this.end.textContent.length);
  };
  R.Param.prototype.validate = R.Translation.prototype.validate;

}(RAPIT));
