var RAPIT = RAPIT || {};

/**
 * RAPIT.registry
 *
 * Keeps a register of all known SourceSets and translations and handles
 * data updates.
 *
 * API:
 * - updateSourceSets( )
 * - addSourceSet( {RAPIT.SourceSet} sourceSet )
 * - getSourceSet( {String|Number} lid )
 * - addTranslation( {Rapit.Translation} translation )
 * - getTranslations( [ {String|Number} lid ] )
 * - getTranslationByOffset( {Number} x, {Number} y )
 */
RAPIT.registry = (function(R, $) {

  var tmp = {
    // Known SourceSet objects
    sourceSets: {},
    // Known Translation objects
    translations: [],
    // Current update status
    loading: false
  };

  /**
   * Retrieves missing sourceSets.
   *
   * Matches the list of known Translations against the known SourceSets
   * and fetches any missing SourceSets from the server.
   *
   * Fired events:
   * - "rapitRegistryUpdateStarted": fires before the AJAX request is issued.
   *   Passed data:
   *     - "required": list of fetched lids
   * - "rapitRegistryUpdateFinished": Fires once all new SourceSets sets have
   *   been added to the registry. Passed data:
   *     - "updated": object containing updated SourceSets,
   *       with updated[lid] = SourceSet
   */
  function updateSourceSets() {
    var required = [], t, i = 0;
    if(tmp.loading) {
      // Do not run multiple concurrent loading operations
      return;
    }
    // Compile a list of missing SourceSets
    while(t = tmp.translations[i++]) {
      if(!tmp.sourceSets.hasOwnProperty(t.lid)) {
        required.push(t.lid);
      }
    }
    if(!required.length) {
      // No update required
      return;
    }
    $(document).trigger('rapitRegistryUpdateStarted', { required: required.length });
    tmp.loading = true;
    R.server.loadSourceSets(required, function(newSourceSets) {
      $.extend(tmp.sourceSets, newSourceSets);
      tmp.loading = false;
      $(document).trigger('rapitRegistryUpdateFinished', { updated: newSourceSets });
    });
  }

  /**
   * Adds or updates a SourceSet.
   *
   * @param {RAPIT.SourceSet} sourceSet
   *   SourceSet instance
   * @param {Boolean} allowUpdate
   *   Update existing SourceSet
   * @throws RapitException
   *   If SourceSet already exists and allowUpdate has not been set
   */
  function addSourceSet(sourceSet, allowUpdate) {
    if(!sourceSet instanceof R.SourceSet) {
      throw new RapitException('Invalid SourceSet');
    }
    if(!getSourceSet(sourceSet.lid) || allowUpdate) {
      tmp.sourceSets[sourceSet.lid] = sourceSet;
    }
    else {
      throw new RapitException('Attempted to add existing SourceSet.');
    }
  }

  /**
   * Returns a SourceSet.
   *
   * @param {Number|String} lid
   *   The SourceSet lid
   * @return RAPIT.SourceSet
   * @return Boolean false
   */
  function getSourceSet(lid) {
    if(tmp.sourceSets.hasOwnProperty(lid)) {
      return tmp.sourceSets[lid];
    }
    return false;
  }

  /**
   * Adds a translation.
   *
   * Does not check for duplicates.
   *
   * @param {RAPIT.Translation} translation
   * @throws RapitException
   *   If translation is not a Translation instance
   */
  function addTranslation(translation) {
    if(!translation instanceof R.Translation) {
      throw new RapitException('Attempted to add invalid translation.');
    }
    tmp.translations.push(translation);
  }

  /**
   * Returns Translations.
   *
   * Either returns all known Translations or only Translations for the
   * given lid.
   *
   * @param {Number|String} lid
   *   SourceSet lid. Optional.
   * @returns Array
   */
  function getTranslations(lid) {
    if(typeof lid === 'undefined') {
      return tmp.translations.slice();
    }
    var candidates = [], i = 0, t;
    while(t = tmp.translations[i++]) {
      if(t.lid == lid) {
        candidates.push(t);
      }
    }
    return candidates;
  }

  /**
   * Returns the best matching Translation for an offset.
   *
   * @param {Number} x
   * @param {Number} y
   * @returns RAPIT.Translation
   * @returns Boolean false
   */
  function getTranslationByOffset(x, y) {
    var i = 0, t, candidates = [], scores = [];
    while(t = tmp.translations[i++]) {
      if(t.matchOffset(x, y)) {
        candidates.push(t);
        scores.push(t.getOffsetScoreX(x));
      }
    }
    if(candidates.length) {
      i = scores.indexOf(Math.max.apply(Math, scores));
      return candidates[i];
    }
    return false;
  }

  // Exposed functions
  return {
    updateSourceSets:       updateSourceSets,
    addSourceSet:           addSourceSet,
    getSourceSet:           getSourceSet,
    addTranslation:         addTranslation,
    getTranslations:        getTranslations,
    getTranslationByOffset: getTranslationByOffset,
    // For debugging only. Do not use.
    translations: tmp.translations,
    sourceSets: tmp.sourceSets
  };
}(RAPIT, jQuery));
