var RAPIT = RAPIT || {};

/**
 * RAPIT.tracker
 *
 * Tracks the mouse position.
 *
 * @todo handle cursor movements when window loses focus
 */
RAPIT.tracker = (function(R, $) {

  var initialized = false, offset = { x: -1, y: -1 };

  function track(event) {
    offset = {
      x: event.clientX || event.pageX,
      y: event.clientY || event.pageY
    };
  }

  function getOffset() {
    return { x: offset.x, y: offset.y };
  }

  function init() {
    if(!initialized) {
      $(window).bind("mousemove mousedown", track);
      initialized = true;
    }
  }

  return {
    init: init,
    getOffset: getOffset
  };

}(RAPIT, jQuery));
