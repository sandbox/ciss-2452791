var RAPIT = RAPIT || {};

/**
 * RAPIT.server
 *
 * Handles client/server communication.
 *
 * API:
 * - loadSourceSets( {Array} lids, {Function} success [, {Function} failure ] )
 */
RAPIT.server = (function(R, $) {

  /**
   * Ajax helper.
   *
   * @param {String} path
   * @param {Object} data
   * @param {Object} options
   */
  function ajax(path, data, options) {
    var baseOptions = {
      url:       Drupal.settings.basePath + path,
      type:     'POST',
      dataType: 'json',
      data:     { data: data }
    };
    $.ajax($.extend(baseOptions, options));
  }

  /**
   * Loads SourceSets from the server.
   *
   * @param {Array} lids
   * @param {Function} success
   *   Success callback, gets passed an object containing SourceSets
   * @param {Function} [failure]
   *   Failure callback
   */
  function loadSourceSets(lids, success, failure) {
    if(!lids.length) {
      success({});
      return;
    }
    ajax('rapit/sources', lids, { failure: failure, success: function(data) {
      var sourceSets = {}, source, i = 0;
      while(source = data[i++]) {
        sourceSets[source.lid] = new R.SourceSet(source.lid, source.context, source.source);
      }
      success(sourceSets);
    }});
  }

  //function loadTranslation(lid, params, success, failure) {
  //  var data = { lid: lid, params: params };
  //  load('rapit/translation', data, { failure: failure, success: function() {
  //    success(new R.Translation)
  //  }});
  //}

  // Exposed functions
  return {
    //loadTranslation: loadTranslation,
    loadSourceSets:  loadSourceSets
  }

}(RAPIT, jQuery));
