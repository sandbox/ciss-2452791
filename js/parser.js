var RAPIT = RAPIT || {};

/**
 * RAPIT.parser
 *
 * Searches for and sanitizes translation strings.
 *
 * Found translations and their node ranges are automatically passed
 * to RAPIT.registry.
 */
RAPIT.parser = (function(R, $) {

  /**
   * Helper object to collect and remove nodes.
   */
  var maid = (function() {
    var nodes = [];

    function handle(node) {
      nodes.push(node);
    }

    function clean() {
      var n;
      while (n = nodes.pop()) {
        if(n.parentNode) {
          n.parentNode.removeChild(n);
        }
        else {
          console.log("Maid: no parent", n);
        }
      }
    }

    return {
      handle: handle,
      clean:  clean
    };
  }());

  /**
   * Provides detection of and easy access to bound marks.
   *
   * @param boundMarks
   *   Object containing the properties "start", "id", "end", "paramStart",
   *   "paramId", "paramEnd"
   * @constructor
   */
  var BoundPattern = function(boundMarks) {

    // Escape special characters
    function escape(str) {
      return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }

    // Wrap every marker in a non-capturing group
    function wrap(str) {
      return '(?:' + str + ')';
    }

    function compile(markers) {
      return new RegExp( markers.map(escape).map(wrap).join('|'), 'g' );
    }

    this.start = boundMarks.start;
    this.id    = boundMarks.id;
    this.end   = boundMarks.end;
    this.paramStart = boundMarks.param_start;
    this.paramId    = boundMarks.param_id;
    this.paramEnd   = boundMarks.param_end;

    this.pattern = compile([ this.start, this.id, this.end, this.paramStart, this.paramId, this.paramEnd ]);
  };

  /**
   * Proxies the internal pattern's exec() function.
   *
   * @param str
   * @returns {Array|{index: number, input: string}}
   */
  BoundPattern.prototype.match = function(str) {
    return this.pattern.exec(str);
  };

  /**
   * Resets the internal index.
   */
  BoundPattern.prototype.reset = function() {
    this.pattern.lastIndex = 0;
  };


  function handleBounds(boundNode, boundPattern, translations) {
    switch(boundNode.nodeValue) {

      // Range start
      case boundPattern.start:
        translations.current = {
          start: boundNode.nextSibling,
          parent: translations.current,
          params: []
        };
        translations.all.push(translations.current);
        return;

      // Range id
      case boundPattern.id:
        // Nothing to do here, id node gets handled by range end
        return;

      // Range end
      case boundPattern.end:
        if(translations.current) {
          translations.current.id = boundNode.previousSibling.nodeValue;
          // Past the end boundNode, the id node, and the id boundNode
          translations.current.end = boundNode.previousSibling.previousSibling.previousSibling;
          translations.current = translations.current.parent;
        }
        // Remove the id node
        maid.handle(boundNode.previousSibling);
        return;

      // Param start
      case boundPattern.paramStart:
        if(translations.current) {
          translations.current.currentParam = { start: boundNode.nextSibling };
        }
        return;

      // Param id
      case boundPattern.paramId:
        // Nothing to do here, id node gets handled by param end
        return;

      // Param end
      case boundPattern.paramEnd:
        if(translations.current) {
          translations.current.params.push(new R.Param(
            boundNode.previousSibling.nodeValue.replace(/^([%!@]):/, '$1'),
            translations.current.currentParam.start,
            boundNode.previousSibling
          ));
          translations.current.currentParam = null;
        }
        maid.handle(boundNode.previousSibling);
        return;

      default:
        throw new RapitException('Unhandled bound: ' + '"' + boundNode.nodeValue + '"');
    }
  }

  /**
   * Extracts a single bound marker from text node.
   *
   * Splits a text node before and after a found bound marker and marks the
   * containing node for removal. The function then passes the node on for
   * further processing and moves the walker forward.
   *
   * @param walker
   *   TreeWalker object
   * @param boundPattern
   * @param translations
   */
  function extractBound(walker, boundPattern, translations) {
    var match, boundNode;
    // Reset pattern index
    boundPattern.reset();
    if(match = boundPattern.match(walker.currentNode.nodeValue)) {
      // Split before the bound, keep the node containing the bound marker
      boundNode = walker.currentNode.splitText(match.index);
      // Split after the bound
      boundNode.splitText(match[0].length);
      // Pass off bound specific handling
      handleBounds(boundNode, boundPattern, translations);
      // Skip bound node
      walker.nextNode();
      // Mark bound node for removal
      maid.handle(boundNode);
    }
  }


  function detectTranslations(node, boundPattern) {

    var
    // Searchable attributes
      attributes = ['title', 'alt', 'href', 'value'],
    // Found translations
      translations = { current: null, all: [] },
      filter, walker, attrWalker, a, i;

    // Workaround for IE9 bug
    filter = function() { return NodeFilter.FILTER_ACCEPT; };
    // Format according to specs
    filter.acceptNode = filter;

    // Parse text nodes
    walker = document.createTreeWalker(node, NodeFilter.SHOW_TEXT, filter, false);
    while(walker.nextNode()) {
      extractBound(walker, boundPattern, translations);
    }

    // Parse attribute nodes
    walker = document.createTreeWalker(node, NodeFilter.SHOW_ELEMENT, filter, false);
    while(walker.nextNode()) {
      for(i = 0; a = attributes[i++];) {
        if(!walker.currentNode.hasAttribute(a)) {
          continue;
        }
        attrWalker = document.createTreeWalker(walker.currentNode.getAttributeNode(a), NodeFilter.SHOW_ALL, filter, false);
        while(attrWalker.nextNode()) {
          extractBound(attrWalker, boundPattern, translations);
        }
      }
    }

    maid.clean();
    return translations.all;
  }


  function parse(node, boundMarks) {
    var translations, t, i = 0, boundPattern = new BoundPattern(boundMarks);
    $(document).trigger('rapitParserStarted', { node: node });
    translations = detectTranslations(node, boundPattern);
    while(t = translations[i++]) {
      RAPIT.registry.addTranslation(new R.Translation(t.id, t.params, t.start, t.end));
    }
    $(document).trigger('rapitParserFinished', { node: node });
  }

  return {
    parse: parse
  }
}(RAPIT, jQuery));
