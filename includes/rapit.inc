<?php

if(!interface_exists('JsonSerializable')) {
  // Dummy interface for PHP < 5.4
  interface JsonSerializable { }
}

class RapitContainer implements ArrayAccess, Serializable, JsonSerializable {

  protected $data;
  protected $index;

  public function __construct(array $data = array(), $index = null) {
    $this->index = $index;
    foreach($data as $offset => $value) {
      $this->offsetSet($offset, $value);
    }
  }

  protected function initContainer($value, $offset) {
    return new RapitContainer($value, $offset);
  }

  public function offsetExists($offset) {
    return isset($this->data[$offset]);
  }

  public function offsetGet($offset) {
    if(!isset($this->data[$offset])) {
      $this->data[$offset] = $this->initContainer(array(), $offset);
    }
    $value = $this->data[$offset];
    return $value;
  }

  public function offsetSet($offset, $value) {
    if (is_array($value)) {
      $value = $this->initContainer($value, $offset);
    }
    if(is_null($offset)) {
      $this->data[] = $value;
    }
    else {
      $this->data[$offset] = $value;
    }
  }

  public function offsetUnset($offset) {
    unset($this->data[$offset]);
  }

  public function serialize() {
    return serialize($this->data);
  }

  public function unserialize($serialized) { }

  public function jsonSerialize() {
    return $this->data;
  }

}


class RapitLocale extends RapitContainer {

  protected function initContainer($value, $offset) {
    return new RapitLanguage($value, $offset);
  }

}


class RapitLanguage extends RapitContainer {

  protected function initContainer($value, $offset) {
    return new RapitContext($value, $offset);
  }

}


class RapitContext extends RapitContainer {

  public function offsetExists($offset) {
    return parent::offsetExists($offset);
  }

  public function offsetGet($offset) {
    if(!isset($this->data[$offset])) {
      throw new Exception('Invalid nesting level for locale');
    }
    return (string) $this->data[$offset];
  }

  public function offsetSet($offset, $value) {
    if (is_array($value)) {
      throw new Exception('Invalid nesting level for locale');
    }
    $value = new RapitString($offset, $value, $this->index);
    if(is_null($offset)) {
      $this->data[] = $value;
    }
    else {
      $this->data[$offset] = $value;
    }
  }
}


/**
 * Class RapitString
 *
 * @todo better property/variable names ("context")
 */
class RapitString {

  static protected $sourceSets = array();
//  static protected $paramSets = array();

  protected $translated;
  protected $tLid;
  protected $tContext;
  protected $tSource;
  protected $translation;

  public function __construct($tSource, $translation, $tContext) {
    $this->translated  = $translation !== true;
    $this->tLid        = rapit_get_locale_lid($tContext, $tSource);
    $this->tContext    = $tContext;
    $this->tSource     = $tSource;
    $this->translation = $this->translated ? $translation : $tSource;
  }

  /**
   * Returns the source string.
   *
   * @return string
   */
  public function getSource() {
    return $this->tSource;
  }

  public function getTranslation() {
    return $this->translation;
  }

  static public function getSourceSets() {
    $sets = array();
    foreach(self::$sourceSets as $lid => $set) {
      $sets[] = array('lid' => $lid) + $set;
    }
    return $sets;
  }

//  static public function getParamSets() {
//    $sets = array();
//    foreach(self::$paramSets as $lid => $lidParamSets) {
//      foreach(array_values($lidParamSets) as $paramsId => $params) {
//        $sets[] = array(
//          'lid' => $lid,
//          'paramsId' => $paramsId,
//          'params' => $params
//        );
//      }
//    }
//    return $sets;
//  }

  protected function register(array $params = array()) {

    if(!(isset(self::$sourceSets[$this->tLid]))) {
      self::$sourceSets[$this->tLid] = array(
        'context'    => $this->tContext,
        'source'     => $this->tSource,
      );
    }

    $paramsId = null;
//    if($params) {
//      $paramsHash = md5(json_encode($params));
//      self::$paramSets[$this->tLid][$paramsHash] = $params;
//      $paramsId = array_search($paramsHash, self::$paramSets);
//    }

    return array($this->tLid, $paramsId);
  }

  public function __toString() {
    $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

    // Strip every call up to t() and the last two calls (as they are always
    // present, and check if we're allowed to alter the string or retrieve arguments.
    if(!rapit_is_valid_trace(array_slice($trace, 4, -2))) {
      return $this->translation;
    }

    // Get a fresh trace to check for placeholder arguments;
//    $trace = debug_backtrace(null);
//    list(, $args) = $trace[3]['args'] + array('', array(),  '');
    $args = array();
    $bounds = rapit_get_bound_markers();

    list($baseId, $paramsId) = $this->register($args);
    $id = is_null($paramsId) ? $baseId : "$baseId:$paramsId";
    $translation = rapit_translation_mark_params($this->translation);
    $value = $bounds['start'] . $translation . $bounds['id'] . $id . $bounds['end'];
    return $value;
  }
}